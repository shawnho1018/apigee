#!/bin/bash
source env.sh
export APIGEECTL_HOME=/Users/shawnho/workspace/apigee/apigee-tools
cd hybrid-files/
${APIGEECTL_HOME}/apigeectl init -f overrides/overrides.yaml
kubectl wait --for=condition=ready pod -l app=apigee-controller -n apigee-system
${APIGEECTL_HOME}/apigeectl apply -f overrides/overrides.yaml
