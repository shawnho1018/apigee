#!/bin/bash
cat << EOF > cassandra-replication.yaml
apiVersion: apigee.cloud.google.com/v1alpha1
kind: CassandraDataReplication
metadata:
  name: region-expansion
  namespace: apigee
spec:
  organizationRef: cht-apigee-demo-f4fc264 
  force: true
  source:
    region: "dc-1"
EOF
