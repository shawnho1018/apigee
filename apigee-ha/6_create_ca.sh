#!/bin/bash
source env.sh
gcloud container clusters get-credentials ${CLUSTERTW} --zone "${GCP_REGION}-a" 
kubectl -n cert-manager get secret apigee-ca -o yaml > apigee-ca.yaml
kubectx gke_${PROJECT_ID}_${ZONE}_${CLUSTER}
kubectl apply -f apigee-ca.yaml
