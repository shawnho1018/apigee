# Multi-Regional Expansion
[This work](https://cloud.google.com/apigee/docs/hybrid/v1.7/multi-region) is to create a high-available runtime plane in a separate region.

## Pre-requisite
Please follow the parent folder's README.md to deploy a ready-to-use apigee org and a runtime plane first. This work is designed based on a runnning apigee org & one gke cluster with apigee runtime deployed. 

## What needs to be further configured? 
* env.sh: Please change the env.sh according to your environment. 
* 3_create_cluster.sh: Used to create a new cluster in your preferred zone (e.g. asia-northeast1-a in this example).
* 4_install_asm.sh: Install ASM. Please remember to use a docker container to simulate linux if you're using mac or windows. asmcli CLI only supports linux. 
* 6_create_ca.sh: It is required to export ca used by apigee-ca from cert-manager namespace and input into the new cluster's cert-manager namespace. 
* 7_create_apigee_config.sh: Used to create apigeectl's config yaml. The generated config file would be placed in ../hybrid-files/overrides/overrides-${cluster_name}.yaml. Please remember to update **multiRegionSeedHost** with the ip address of the cassandra pod in source cluster.
* 8_install_apigee.sh: Using apigeectl to install the config yaml from step 7.
* 9_config_cassandra_replication.sh: Generate a cassandra replication yaml for operator. The resulting file will be placed in the same apigee-ha folder with cassandra-replication.yaml. Once the file is ready, please use
```
kubectx # to new cluster
kubectl apply -f cassandra-replication.yaml
```
* 98_remove_apigee.sh: Using apigeectl to remove apigee runtime installation.

