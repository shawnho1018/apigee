#!/bin/bash
export PROJECT_ID="cht-apigee-demo"
export GCP_REGION="asia-east1"
export REGION="asia-northeast1"
export ZONE="${REGION}-a"
export DOMAIN=shawnk8s.com
export ENV_GROUP="cht-mobile"
export CLUSTERTW="apigee-tw"
export CLUSTER="apigee-jp"
export NETWORK="apigee"
export SUBNET="apigee-subnet"
export ORG_NAME="${PROJECT_ID}"
export ORG_DISPLAY_NAME="cht-test"
export RGANIZATION_DESCRIPTION="An org for CHT's test"
export ANALYTICS_REGION=${REGION}
export RUNTIMETYPE=HYBRID
export LONG_RUNNING_OPERATION_ID="557988df-ea3a-4a6d-b30f-3f135b639bd4"
export TOKEN=$(gcloud auth print-access-token)
gcloud config set project ${PROJECT_ID}

# Specify Env & EnvGroup Parameters
# Specify Env & EnvGroup Parameters
export ENV_NAME="cht-mobile-app1"
export ENV_NAME2="cht-map"
export ENV_DISPLAY_NAME=${ENV_NAME}
export ENV_DISPLAY_NAME2=${ENV_NAME2}
export ENV_DESCRIPTION="This env is for ${ENV_NAME} team"
export ENV_DESCRIPTION2="This env is for ${ENV_NAME2} team"
