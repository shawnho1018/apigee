#!/bin/bash
source env.sh
#gcloud compute networks create ${NETWORK}
gcloud compute networks subnets create ${SUBNET} --network ${NETWORK} --range 10.1.1.0/24 --region ${REGION}

gcloud compute project-info add-metadata --metadata enable-oslogin=FALSE
gcloud container clusters create ${CLUSTER} --zone ${ZONE} --network ${NETWORK} --subnetwork ${SUBNET} --workload-pool="${PROJECT_ID}.svc.id.goog" --shielded-secure-boot --shielded-integrity-monitoring --num-nodes=1 

gcloud container node-pools delete --quiet default-pool --cluster ${CLUSTER} --zone ${ZONE}
gcloud container node-pools create --quiet apigee-data --cluster ${CLUSTER} --machine-type e2-standard-4 --num-nodes=3 --zone ${ZONE} --shielded-secure-boot --shielded-integrity-monitoring 
gcloud container node-pools create --quiet apigee-runtime --cluster ${CLUSTER} --machine-type e2-standard-4 --num-nodes=3 --zone ${ZONE} --shielded-secure-boot --shielded-integrity-monitoring

# install cert-manager
gcloud container clusters get-credentials ${CLUSTER} --zone ${ZONE}
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.7.2/cert-manager.yaml
