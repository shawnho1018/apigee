#!/bin/bash
source env.sh
export APIGEECTL_HOME=../apigee-tools
cd ../hybrid-files/
${APIGEECTL_HOME}/apigeectl delete -f overrides/overrides-${CLUSTER}.yaml
