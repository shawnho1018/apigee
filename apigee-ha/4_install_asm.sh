#!/bin/bash
source env.sh
./asmcli install \
  --verbose \
  --project_id $PROJECT_ID \
  --cluster_name $CLUSTER \
  --cluster_location $ZONE \
  --output_dir /root/asm-install-$CLUSTER \
  --custom_overlay overlay.yaml \
  --enable_all \
  --option legacy-default-ingressgateway
