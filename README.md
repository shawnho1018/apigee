# Apigee Playground
## Introduction
This project is to install a workable apigee hybrid distribution from 0. There are some manual processes included and would be documented here. 
### Note: Argolis User
Please Allow the following Org Policies to run our script. 
* vmExternalIpAccess: Allow All
* compute.requireOsLogin: Disable All
* disableServiceAccountKeyCreation: Not Enforced
## Version Information:
	* Kubernetes 1.22
	* Apigee 1.7.2
	* ASM 1.12.7-asm.2+config1
## Detailed Explanation
* env.sh: Most of the environmental variables are documented in this file. Try to source env.sh in the beginning of other script.
* 0_api.sh: Enable google apis for Apigee
* 1_create_org.sh: Create Apigee Org 
* 2_create_env.sh: Using API to create environment/environment group on Apigee console. This can also be created using apigee console UI. However, the real "env" will not be created until step 8. 
* 3_create_cluster.sh: Create a GKE cluster. In order to fulfill apigee's requirements, we will delete default-pool and produce apigee-data & apigee-runtime worker pools. 
* 4_install_asm.sh + overlay.yaml: Install Anthos service mesh. Please note that MacOS is not supported. If you're using mac, please use the following script to install. 
```
docker run -it --rm -v $(pwd):/data google/cloud-sdk:latest  /bin/bash
``` 
  After entering the container shell, please use gcloud auth login to set up your credentials and source env.sh to set your project. After everything is done, please run 4_install_asm.sh. 

* 5_prepare_apigee.sh: Create service accounts for latter apigee installation.
* 6_create_ca.sh: Create certificate and key for apigee hostname. 
* 7_create_apigee_config.sh: Create apigee config file under hybrid-files/overrides/overrides.yaml. If additional env is needed, please fix the overrides.yaml. 
* 8_install_apigee.sh: Use apigeectl init & apigeectl apply to create apigee runtime. 

## Misc Tool
* query_job.sh is a tool script used to query the long running job such as create_org and delete_org. This script takes 1 input parameter which could be found after running the curl request
```
{
  "name": "organizations/cht-apigee-demo/operations/0a63f41e-de24-4958-96e3-e821990d119c",
  "metadata": {
    "@type": "type.googleapis.com/google.cloud.apigee.v1.OperationMetadata",
    "operationType": "INSERT",
# Apigee Playground
    "targetResourceName": "organizations/cht-apigee-demo",
# Apigee Playground
    "state": "IN_PROGRESS"
  }
}
```
0a63f41e-de24-4958-96e3-e821990d119c is the input parameter. 
