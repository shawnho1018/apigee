#!/bin/bash
source env.sh
curl -X DELETE -H "Authorization: Bearer $TOKEN" "https://apigee.googleapis.com/v1/organizations/${ORG_NAME}"
