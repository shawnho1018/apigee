#!/bin/bash
export APIGEECTL_HOME=$(pwd)/apigee-tools
export VERSION=$(curl -s \
    https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/current-version.txt?ignoreCache=1)
curl -LO https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/$VERSION/apigeectl_linux_64.tar.gz
tar zxvf apigeectl_linux_64.tar.gz -C ./
mv apigeectl_1.7.2-e080e04_linux_64 apigeectl-tools

mkdir -p hybrid-files
mkdir -p hybrid-files/overrides
mkdir -p hybrid-files/certs

cd hybrid-files/
ln -s $APIGEECTL_HOME/tools tools
ln -s $APIGEECTL_HOME/config config
ln -s $APIGEECTL_HOME/templates templates
ln -s $APIGEECTL_HOME/plugins plugins


./tools/create-service-account --env prod --dir ./service-accounts

curl -X POST -H "Authorization: Bearer ${TOKEN}" \
  -H "Content-Type:application/json" \
  "https://apigee.googleapis.com/v1/organizations/${ORG_NAME}:setSyncAuthorization" \
   -d '{"identities":["'"serviceAccount:apigee-synchronizer@${ORG_NAME}.iam.gserviceaccount.com"'"]}'
