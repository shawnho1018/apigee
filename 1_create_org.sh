#!/bin/bash
source env.sh
curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json" \
  -d '{
    "name":"'"$ORG_NAME"'",
    "displayName":"'"$ORG_DISPLAY_NAME"'",
    "description":"'"$ORGANIZATION_DESCRIPTION"'",
    "runtimeType":"'"$RUNTIMETYPE"'",
    "analyticsRegion":"'"$ANALYTICS_REGION"'"
  }' \
  "https://apigee.googleapis.com/v1/organizations?parent=projects/$PROJECT_ID"
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member user:shawnho@google.com \
  --role roles/apigee.admin
