#!/bin/bash
source env.sh
mkdir -p hybrid-files/certs/
openssl req  -nodes -new -x509 -keyout ./hybrid-files/certs/keystore.key -out \
    ./hybrid-files/certs/keystore.pem -subj '/CN='$DOMAIN'' -days 3650
