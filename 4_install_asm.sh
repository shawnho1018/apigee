#!/bin/bash
source env.sh
./asmcli install \
  --verbose \
  --project_id $PROJECT_ID \
  --cluster_name $CLUSTERTW \
  --cluster_location $ZONE \
  --output_dir /root/asm-install-${CLUSTERTW} \
  --custom_overlay overlay.yaml \
  --enable_all \
  --option legacy-default-ingressgateway
