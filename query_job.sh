#!/bin/bash
source env.sh
#export LONG_RUNNING_OPERATION_ID="4f2376fa-9d52-4c64-bfad-03c674167eeb"
export LONG_RUNNING_OPERATION_ID=$1
curl -H "Authorization: Bearer $TOKEN" \
  "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/operations/$LONG_RUNNING_OPERATION_ID"
