#!/bin/bash
source env.sh

# create environment
curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json"   -d '{
    "name": "'"$ENV_NAME"'",
    "displayName": "'"$ENV_DISPLAY_NAME"'",
    "description": "'"$ENV_DESCRIPTION"'"
  }'   "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/environments"

curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json"   -d '{
    "name": "'"$ENV_NAME2"'",
    "displayName": "'"$ENV_DISPLAY_NAME2"'",
    "description": "'"$ENV_DESCRIPTION2"'"
  }'   "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/environments"
# create environment group
curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json" \
   -d '{
     "name": "'"$ENV_GROUP"'",
     "hostnames":["'"$DOMAIN"'"]
   }' \
   "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/envgroups"
# Add env1 into env group
curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json" \
   -d '{
     "environment": "'"$ENV_NAME"'",
   }' \
   "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/envgroups/$ENV_GROUP/attachments"
# Add env2 into env group
curl -H "Authorization: Bearer $TOKEN" -X POST -H "content-type:application/json" \
   -d '{
     "environment": "'"$ENV_NAME2"'",
   }' \
   "https://apigee.googleapis.com/v1/organizations/$ORG_NAME/envgroups/$ENV_GROUP/attachments"
