#!/bin/bash
source env.sh
cat << EOF > hybrid-files/overrides/overrides.yaml
gcp:
  region: ${REGION}
  projectID: ${PROJECT_ID}

k8sCluster:
  name: ${CLUSTERTW}
  region: ${REGION} # Must be the closest Google Cloud region to your cluster.
org: ${ORG_NAME}

instanceID: "${CLUSTERTW}-1"  # See the property description table below information about this parameter.
  
cassandra:
  hostNetwork: false
  replicaCount: 1
  datacenter: dc-1
    # Set to false for single region installations and multi-region installations
    # with connectivity between pods in different clusters, for example GKE installations.
    # Set to true  for multi-region installations with no communication between
    # pods in different clusters, for example GKE On-prem, GKE on AWS, Anthos on bare metal,
    # AKS, EKS, and OpenShift installations.
    # See Multi-region deployment: Prerequisites

virtualhosts:
  - name: ${ENV_GROUP}
    sslCertPath: ./certs/keystore.pem
    sslKeyPath: ./certs/keystore.key

envs:
  - name: ${ENV_NAME}
    serviceAccountPaths:
      synchronizer: ./service-accounts/${PROJECT_ID}-apigee-synchronizer.json 
        # for production environments, ${PROJECT_ID}-apigee-synchronizer.json
      udca: ./service-accounts/${PROJECT_ID}-apigee-udca.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-udca.json
      runtime: ./service-accounts/${PROJECT_ID}-apigee-runtime.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-runtime.json
  - name: ${ENV_NAME2}
    serviceAccountPaths:
      synchronizer: ./service-accounts/${PROJECT_ID}-apigee-synchronizer.json
        # for production environments, ${PROJECT_ID}-apigee-synchronizer.json
      udca: ./service-accounts/${PROJECT_ID}-apigee-udca.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-udca.json
      runtime: ./service-accounts/${PROJECT_ID}-apigee-runtime.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-runtime.json
mart:
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-mart.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-mart.json

connectAgent:
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-mart.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-mart.json
        # Use the same service account for mart and connectAgent

metrics:
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-metrics.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-metrics.json

udca:
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-udca.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-udca.json

watcher:
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-watcher.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-watcher.json

logger:
  enabled: false
        # Set to false to disable logger for GKE installatinos.
        # Set to true for all platforms other than GKE.
        # See apigee-logger in Service accounts and roles used by hybrid components.
  serviceAccountPath: ./service-accounts/${PROJECT_ID}-apigee-logger.json
        # for non-production environments, ${PROJECT_ID}-apigee-non-prod.json
        # for production environments, ${PROJECT_ID}-apigee-logger.json
EOF
